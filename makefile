build:
	docker-compose build

clone:
	docker-compose run --rm dat-cli clone .

sync:
	docker-compose run --rm dat-cli sync .